package EA3;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class EA3 {
    private static int contador=0;
    public static void main(String[] args) throws IOException {
        File f = new File("Departaments.dat");
        menu(f);
    }

    public static void menu(File f) throws IOException {
        System.out.println("Introdueix l'exercici a consultar: \n[1] Crear fitxer\n[2] Modificar departament\n[3] Eliminar Departament\n[4] Llegir gitxer\n[0] Per sortir del programa");
        int opcio;
        Scanner lector = new Scanner(System.in);
        opcio = lector.nextInt();
        switch (opcio) {
            case 1:
                crearFitxer(f);
                menu(f);
            case 2:
                modificarDepartament();
                menu(f);
            case 3:
                f = eliminarDepartament();
                menu(f);
            case 4:
                llegirFitxer();
                menu(f);
            case 0:
                break;
            default:
                System.out.println("Aquesta opció no es valida");
        }
    }

    private static File eliminarDepartament() throws IOException {
        File f = new File("Departaments.dat");
        Scanner lector = new Scanner(System.in);
        File f2 = new File("departamentscopia2.dat");
        if (f2.exists()) {
            f2.delete();
            f2.createNewFile();
        }
        int id;
        System.out.println("Introdueix el id a eliminar");
        id = lector.nextInt();
        contador=0;
        if (comprobarExistenciaDept(id)) {
            try {
                String guardar1, guardar2;
                FileInputStream fis = new FileInputStream(f);
                FileOutputStream fos = new FileOutputStream(f2);
                DataInputStream dis = new DataInputStream(fis);
                DataOutputStream dos = new DataOutputStream(fos);
                int i = 0, id2;
                while (i < dis.available()) {
                    id2 = dis.readInt();
                    if (id2 == id) {
                        dis.readUTF();
                        dis.readUTF();
                    } else {
                        dos.writeInt(id2);
                        guardar1 = dis.readUTF();
                        guardar2 = dis.readUTF();
                        dos.writeUTF(guardar1);
                        dos.writeUTF(guardar2);
                    }
                }
                dos.close();
                dis.close();
                System.out.println("Departament eliminat");
                System.out.println("Hi han: "+(contador-1)+" departaments");
                File aux = new File(f.getName());
                f.delete();
                f2.renameTo(aux);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return f2;
        } else {
            System.out.println("Aquest departament no existeix");
            return f;
        }
    }

    public static boolean comprobarLongitud(String paraula) {
        if (paraula.length() > 30) {
            System.out.println("Error: aquesta paraula es massa llarga");
            return false;
        } else {
            return true;
        }
    }

    public static boolean comprobarExistenciaDept(int id) {
        int i = 0;
        boolean comprobar = false;
        int numero;
        try {
            File f = new File("Departaments.dat");
            DataInputStream dis = new DataInputStream(new FileInputStream(f));
            if (!f.exists()) {
                System.out.println("Aquests fitxer no existeix");
            }
            while (dis.available() > 0) {
                numero = dis.readInt();
                if (numero == id) {
                    comprobar = true;
                }
                dis.readUTF();
                dis.readUTF();
                contador++;
            }
            dis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return comprobar;
    }

    private static void modificarDepartament() throws IOException {
        Scanner lector = new Scanner(System.in);
        File fc = new File("Departaments.dat");
        File f2 = new File("departamentscopia.dat");
        f2.delete();
        f2.createNewFile();
        int id;
        String nouNom, novaLocalitat;
        System.out.println("Introdueix el id a modificar");
        id = lector.nextInt();
        if (comprobarExistenciaDept(id)) {
            System.out.println("Introdueix el nou nom");
            nouNom = lector.next();
            while (!comprobarLongitud(nouNom)) {
                System.out.println("Torna a introduir el nom");
                nouNom = lector.next();
            }
            System.out.println("Introdueix la nova localitat");
            novaLocalitat = lector.next();
            while (!comprobarLongitud(novaLocalitat)) {
                System.out.println("Torna a introduir la localitat");
                novaLocalitat = lector.next();
            }
            try {
                String guardar1, guardar2;
                FileInputStream fis = new FileInputStream(fc);
                FileOutputStream fos = new FileOutputStream(f2);
                DataInputStream dis = new DataInputStream(fis);
                DataOutputStream dos = new DataOutputStream(fos);
                int i = 0, id2;
                System.out.println(dis.available());
                while (i < dis.available()) {
                    id2 = dis.readInt();
                    if (id2 == id) {
                        dos.writeInt(id);
                        dis.readUTF();
                        dis.readUTF();
                        dos.writeUTF(nouNom);
                        dos.writeUTF(novaLocalitat);
                    } else {
                        dos.writeInt(id2);
                        guardar1 = dis.readUTF();
                        guardar2 = dis.readUTF();
                        dos.writeUTF(guardar1);
                        dos.writeUTF(guardar2);
                    }
                }
                dos.close();
                dis.close();
                System.out.println("Departament canviat");
                File aux = new File(fc.getName());
                fc.delete();
                f2.renameTo(aux);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Aquest departament no existeix");
        }
    }

    private static void llegirFitxer() {
        File f = new File("Departaments.dat");
        try {
            if (!f.exists()) {
                System.out.println("Aquests fitxer no existeix");
            }
            FileInputStream fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            while (dis.available() > 0) {
                System.out.print(dis.readInt() + " ");
                System.out.print(dis.readUTF() + " ");
                System.out.print(dis.readUTF());
                System.out.println();
            }
            fis.close();
            dis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void crearFitxer(File f) {
        int[] departmentId = {10, 20, 50, 40, 60, 70, 76, 12, 11, 90};
        String[] nom = {"finances", "marketing", "inventari", "it", "sistemas", "programacio", "disseny", "contabilitat", "ventas", "compres"};
        String[] localitat = {"barcelona", "barcelona", "reus", "madrid", "sevilla", "granada", "barcelona", "berlin", "paris", "mexic"};
        try {
            if (!f.exists()) {
                f.createNewFile();
            } else {
                f.delete();
                f.createNewFile();
            }
            DataOutputStream dos = new DataOutputStream(new FileOutputStream(f));
            for (int i = 0; i < departmentId.length; i++) {
                dos.writeInt(departmentId[i]);
                dos.writeUTF(nom[i]);
                dos.writeUTF(localitat[i]);
            }
            dos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
