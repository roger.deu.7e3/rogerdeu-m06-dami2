package P1;

import java.io.*;

import Pojo.*;

public class P1 {

    public static void main(String[] args) {
        File arxiuEmpleats = new File("Empleats.dat");
        File arxiuDepartaments = new File("Departaments.dat");
        if (arxiuDepartaments.exists()){
            arxiuDepartaments.delete();
        }
        if (arxiuEmpleats.exists()) {
            arxiuEmpleats.delete();
        }
        ex2();
        ex3();
        ex2();
        ex3();
    }

    public static void ex2() {
        String[] nomsEmpleats = {"Joan","Josep","Anna","Anastasia","Roger"};
        int[] idDepartaments = {10,30,20,10,50};
        int[] idEmpleat = {1,2,3,4,5};
        double[] salari = {20000,35000,40000,18000,15000};
        String[] departamentLoc = {"Barcelona","Girona","Reus","Reus","Lleida"};
        String[] nomDept = {"Sistemes","BBDD","Programacion","RRHH","ERP"};

        File arxiuEmpleats = new File("Empleats.dat");
        File arxiuDepartaments = new File("Departaments.dat");
        Empleat emp;
        Departament d;
        ObjectOutputStream Oempleats;
        ObjectOutputStream Odepartaments;

        try {

            if (!arxiuEmpleats.exists()) {
                System.out.println("no existeix");
                Oempleats = new ObjectOutputStream(new FileOutputStream(arxiuEmpleats));
            } else {
                System.out.println("existeix");
                Oempleats = new MiObjectOutputStream(new FileOutputStream(arxiuEmpleats,true));
            }
            for (int i = 0; i < nomsEmpleats.length; i++) {
                emp = new Empleat(idEmpleat[i],idDepartaments[i],nomsEmpleats[i],salari[i]);
                Oempleats.writeObject(emp);
            }
            Oempleats.close();

            if (!arxiuDepartaments.exists()) {
                Odepartaments = new ObjectOutputStream(new FileOutputStream(arxiuDepartaments));
            } else {
                Odepartaments = new MiObjectOutputStream(new FileOutputStream(arxiuDepartaments,true));
            }

            for (int i = 0; i < departamentLoc.length; i++) {
                d = new Departament(idDepartaments[i],nomDept[i],departamentLoc[i]);
                Odepartaments.writeObject(d);
            }
            Odepartaments.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ex3() {
        File arxiuEmpleats = new File("Empleats.dat");
        File arxiuDepartaments = new File("Departaments.dat");
        Empleat emp;
        Departament dept;

        try {
            ObjectInputStream disE = new ObjectInputStream(new FileInputStream(arxiuEmpleats));

            while (true) {
                emp = (Empleat) disE.readObject();
                System.out.println(emp.toString());
            }

        }catch (Exception e) {
            System.out.println("Arxiu llegit");
        }

        try {
            ObjectInputStream disD = new ObjectInputStream(new FileInputStream(arxiuDepartaments));

            while (true) {
                dept = (Departament) disD.readObject();
                System.out.println(dept.toString());
            }
        } catch (Exception e) {
            System.out.println("Arxiu llegit");
        }
    }

}