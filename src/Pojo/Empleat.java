package Pojo;

import java.io.Serializable;

public class Empleat implements Serializable {
    private int id, departament;
    private String cognom;
    private double salari;

    public Empleat(int id, int departament, String cognom, double salari) {
        this.id = id;
        this.departament = departament;
        this.cognom = cognom;
        this.salari = salari;
    }

    @Override
    public String toString() {
        return "Empleat{" +
                "id=" + id +
                ", departament=" + departament +
                ", cognom='" + cognom + '\'' +
                ", salari=" + salari +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDepartament() {
        return departament;
    }

    public void setDepartament(int departament) {
        this.departament = departament;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public double getSalari() {
        return salari;
    }

    public void setSalari(double salari) {
        this.salari = salari;
    }
}
