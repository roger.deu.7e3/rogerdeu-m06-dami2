package P2;

import Pojo.Empleat;

import java.util.ArrayList;
import java.util.List;

public class ListaEmpleados {
    private List<Empleat> e = new ArrayList<Empleat>();

    public ListaEmpleados() {
    }

    public void add(Empleat emp) {
        e.add(emp);
    }

    public List<Empleat> getE() {
        return e;
    }
}
