package P2;
import Pojo.*;

import java.util.ArrayList;
import java.util.List;

public class ListaDepartamentos {
    private List<Departament> d = new ArrayList<Departament>();

    public ListaDepartamentos() {
    }

    public void add(Departament dept) {
        d.add(dept);
    }

    public List<Departament> getD() {
        return d;
    }
}
