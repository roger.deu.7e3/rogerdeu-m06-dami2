package P2;

import Pojo.Departament;
import Pojo.Empleat;
import com.thoughtworks.xstream.XStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

public class P2 {
    public static void main(String[] args) {
        convertirFitxers();
        llegirXML();
    }

    private static void llegirXML() {
        try {
            XStream xStream = new XStream();
            xStream.alias("ListaDepartamentos",ListaDepartamentos.class);
            xStream.alias("DatosDepartamentos",Departament.class);
            xStream.addImplicitCollection(ListaDepartamentos.class,"d");

            ListaDepartamentos d = (ListaDepartamentos) xStream.fromXML(new FileInputStream("Departamentos.xml"));

            XStream xStream1 = new XStream();
            xStream1.alias("ListaEmpleados",ListaEmpleados.class);
            xStream1.alias("DatosEmpleados",Empleat.class);
            xStream1.addImplicitCollection(ListaEmpleados.class,"e");

            ListaEmpleados e = (ListaEmpleados) xStream1.fromXML(new FileInputStream("Empleados.xml"));

            List<Departament> aD = new ArrayList<>();
            aD = d.getD();

            List<Empleat> aE = new ArrayList<>();
            aE = e.getE();

            aD.forEach(System.out::println);
            aE.forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void convertirFitxers() {
        File fDept = new File("Departaments.dat");
        File fEmp = new File("Empleats.dat");

        try {
            ObjectInputStream oDept = new ObjectInputStream(new FileInputStream(fDept));
            ObjectInputStream oEmp = new ObjectInputStream(new FileInputStream(fEmp));
            ListaDepartamentos listaDepartamentos = new ListaDepartamentos();
            ListaEmpleados listaEmpleados = new ListaEmpleados();

            try {
                while (true) {
                    listaDepartamentos.add((Departament) oDept.readObject());
                }
            } catch (Exception e) {
                System.out.println("Arxiu llegit");
            }

            try {
                while (true) {
                    listaEmpleados.add((Empleat) oEmp.readObject());
                }
            } catch (Exception e) {
                System.out.println("Arxiu llegit");
            }

            try {
                File f = new File("Departamentos.xml");
                if (f.exists()){
                    f.delete();
                    f.createNewFile();
                }
                XStream xStream = new XStream();
                xStream.alias("ListaDepartamentos",ListaDepartamentos.class);
                xStream.alias("DatosDepartamentos",Departament.class);
                xStream.addImplicitCollection(ListaDepartamentos.class,"d");
                xStream.toXML(listaDepartamentos, new FileOutputStream(f));
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                File f = new File("Empleados.xml");
                if (f.exists()){
                    f.delete();
                    f.createNewFile();
                }
                XStream xStream = new XStream();
                xStream.alias("ListaEmpleados",ListaEmpleados.class);
                xStream.alias("DatosEmpleados",Empleat.class);
                xStream.addImplicitCollection(ListaEmpleados.class,"e");
                xStream.toXML(listaEmpleados,new FileOutputStream(f));
            }catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
