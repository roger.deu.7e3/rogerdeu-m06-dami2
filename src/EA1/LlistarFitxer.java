package EA1;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

public class LlistarFitxer {
    public static void main(String[] args) {
       menu();
    }

    public static void menu() {
        Scanner lector = new Scanner(System.in);
        System.out.println("Introdueix l'exercici que vols executar del 1 al 12 o 0 per a sortir");
        switch (lector.nextInt()) {
            case 0:
                break;
            case 1:
                llistarFitxers();
            break;
            case 2:
                omplirFitxer();
            break;
            case 3:
                omplirFitxerAppend();
            break;
            case 4:
                llegirFitxer();
            break;
            case 5:
                llegirFitxerScanner();
            break;
            case 6:
                llegirEnters();
            break;
            case 7:
                obtenirLiniaLc();
            break;
            case 8:
                eliminarFitxer();
            break;
            case 9:
                buscarInfoFitxer();
            break;
            case 10:
                llegirRestaurants();
            break;
            case 11:
                introduirRestaurant();
            break;
            case 12:
                copiarRestaurants();
            break;
        }
    }

    private static void introduirRestaurant() {
        String[] fields = {"Nom","Telefon","Direccio","Districte","Barri","Ciutat","Codi Postal","Regio","Pais","Lat","Lon","Web","Mail"};
        int contador = 0;
        String field;
        try {
            Scanner lector = new Scanner(System.in);
            File f = new File("restaurants.csv");
            FileWriter writer = new FileWriter(f,true);

            while (contador < fields.length){
                System.out.println("Introdueix el/la: "+fields[contador]);
                field = lector.nextLine();
                if (field.isEmpty()){
                    writer.append("---,");
                } else {
                    writer.append(field+",");
                }
                contador++;
            }
            writer.append("\n");
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu();
    }

    private static void copiarRestaurants() {
        File f = new File("restaurants.csv");
        File f2 = new File("restaurants2.csv");
        if (!f2.exists()) {
            try {
                f2.createNewFile();
            } catch (Exception e){

            }
        }
        String frase;
        try {
            Scanner lector = new Scanner(f);
            FileWriter writer = new FileWriter(f2);
            while (lector.hasNext()){
                frase = lector.nextLine();
                if (!frase.contains("Eixample")){
                    writer.write(frase);
                }
            }
            writer.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        menu();
    }

    private static void llegirRestaurants() {
        File f = new File("restaurants.csv");
        int contador=1;
        String frase;
        try {
            Scanner lector = new Scanner(f);
            while (lector.hasNextLine()){
                frase = lector.nextLine();
                if (frase.contains("Eixample")){
                    System.out.println(frase);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu();
    }

    private static void buscarInfoFitxer() {
        Scanner lector = new Scanner(System.in);
        System.out.println("Introdueix la ruta amb el fitxer que vulguis crear");
        String ruta = lector.nextLine();
        File f = new File(ruta);
        if (f.exists()) {
            System.out.println("Si que existeix");
            if (f.isFile()){
                System.out.println("Es un fitxer");
                System.out.println("Mida: "+f.length());
                System.out.println("Nom: "+f.getName());
                if (f.canWrite()){
                    System.out.print("Permisos: escriptura");
                }
                if (f.canRead()) {
                    System.out.print(", lectura");
                }
            } else {
                System.out.println("Es un directori");
            }
        } else {
            System.out.println("Aquest fitxer o directori no existeix");
        }
        menu();
    }

    private static void eliminarFitxer() {
        Scanner lector = new Scanner(System.in);
        System.out.println("Introdueix la ruta amb el fitxer que vulguis crear");
        String ruta = lector.nextLine();
        File f = new File(ruta);
        if (f.exists() && f.isFile()) {
            f.delete();
            System.out.println("Fitxer eliminat");
        } else {
            System.out.println("Aquest fitxer no existeix o es un directori");
        }
        menu();
    }

    private static void obtenirLiniaLc() {
        Scanner lector = new Scanner(System.in);
        System.out.println("Introdueix la ruta amb el fitxer que vulguis crear");
        String ruta = lector.nextLine();
        File f = new File(ruta);
        String mesLlarga = "", mesCurta = "";
        int mesCaracters = 0, menysCaracters = 10;

        try {
            Scanner reader = new Scanner(f);
            while (reader.hasNext()) {
                String paraula = reader.nextLine();
                if (paraula.length()>mesCaracters){
                    mesCaracters=paraula.length();
                    mesLlarga = paraula;
                }
                if (paraula.length() < menysCaracters) {
                    menysCaracters=paraula.length();
                    mesCurta=paraula;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("La paraula mes llarga es: "+mesLlarga);
        System.out.println("La paraula mes curta es: "+mesCurta);
        menu();
    }

    public static void llegirEnters() {
        File f = new File("enters.txt");
        int actual=0, suma=0;
        try {
            Scanner reader = new Scanner(f);
            while (reader.hasNextInt()) {
                int numero = reader.nextInt();
                actual++;
                suma = suma +numero;
                System.out.println(numero);
            }
            System.out.println("hi ha: "+actual+" numeros");
            System.out.println("que entre tots sumen: "+suma);
        } catch (Exception e){
            e.printStackTrace();
        }
        menu();
    }

    public static void llegirFitxerScanner(){
        Scanner lector = new Scanner(System.in);
        System.out.println("Introdueix la ruta amb el fitxer que vulguis crear");
        String ruta = lector.nextLine();
        File f = new File(ruta);
        try {
            if (!f.exists()){
                f.createNewFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Scanner reader = new Scanner(f);
            while (reader.hasNext()){
                System.out.println(reader.nextLine());
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu();
    }

    private static void omplirFitxerAppend() {
        Scanner lector = new Scanner(System.in);
        System.out.println("Introdueix la ruta amb el fitxer que vulguis crear");
        String ruta = lector.nextLine();
        File f = new File(ruta);
        try {
            if (!f.exists()){
                f.createNewFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            String content;
            FileWriter escriptor = new FileWriter(f,true);
            System.out.println("Quantes linies de text vols introduir?");
            int linies = lector.nextInt();
            int i = 0;
            lector.nextLine();
            while (i<linies) {
                System.out.println("Escriu el que vulguis introduir en el fitxer");
                content = lector.nextLine();
                escriptor.append(content);
                escriptor.write("\n");
                i++;
            }
            escriptor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu();
    }

    public static void llistarFitxers(){
        Scanner lector = new Scanner(System.in);
        String ruta;
        System.out.println("Introdueix la ruta per a llistar els fitxers");
        ruta=lector.nextLine();
        File f = new File(ruta);

        if (f.isDirectory() && f.exists()){
            File[] vector = f.listFiles();
            for(File fi:vector) {
                if (fi.isFile()){
                    System.out.println("File: "+ fi.getName());
                } else {
                    System.out.println("Dir: "+fi.getName());
                }
            }
        } else {
            System.out.println("Error ha de ser un directori");
        }
        menu();
    }

    public static void omplirFitxer() {
        Scanner lector = new Scanner(System.in);
        System.out.println("Introdueix la ruta amb el fitxer que vulguis crear");
        String ruta = lector.nextLine();
        File f = new File(ruta);
        try {
            if (!f.exists()){
                f.createNewFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            String content;
            FileWriter escriptor = new FileWriter(f);
            System.out.println("Quantes linies de text vols introduir?");
            int linies = lector.nextInt();
            int i = 0;
            lector.nextLine();
            while (i<linies) {
                System.out.println("Escriu el que vulguis introduir en el fitxer");
                content = lector.nextLine();
                escriptor.append(content);
                escriptor.write("\n");
                i++;
            }
            escriptor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu();
    }

    public static void llegirFitxer() {
        Scanner lector = new Scanner(System.in);
        System.out.println("Introdueix la ruta amb el fitxer que vulguis crear");
        String ruta = lector.nextLine();
        File f = new File(ruta);
        try {
            if (!f.exists()){
                f.createNewFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            FileReader reader = new FileReader(f);
            int c;
            while ((c = reader.read())!=-1) {
                System.out.print( (char) c);
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu();
    }


}
