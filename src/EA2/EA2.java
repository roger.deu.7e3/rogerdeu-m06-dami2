package EA2;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class EA2 {
    public static void main(String[] args) {
        try {
            RandomAccessFile raf = new RandomAccessFile(new File("AleatorioEmple.dat"),"rw");
            menu(raf);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void menu(RandomAccessFile raf) {
        Scanner lector = new Scanner(System.in);
        int opcio;
        System.out.println("Introdueix l'exercici a consultar: \n[1] Consultar empleat\n[2] Introduir empleat\n[3] Augmentar Salari\n[4] Eliminar empleat\n[5] Consultar empleats eliminats\n[0] Per sortir del programa");
        opcio = lector.nextInt();
        switch (opcio) {
            case 1:
                ex1(raf);
                break;
            case 2:
                ex2(raf);
                break;
            case 3:
                ex3(raf);
                break;
            case 4:
                ex4(raf);
                break;
            case 5:
                ex5(raf);
                break;
            case 0:
                break;
        }
    }


    private static void ex5(RandomAccessFile raf) {
        int i = 0;
        try {
            while (i < raf.length()) {
                if (raf.readInt() == -1) {
                    System.out.println("L'id del empleat eliminat es: "+raf.readInt());
                }
                i += 36;
                raf.seek(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu(raf);
    }

    private static void ex4(RandomAccessFile raf) {
        Scanner lector = new Scanner(System.in);
        int id, i=0;
        boolean existeix = false;
        System.out.println("Introdueix l'id d'empleat a eliminar");
        id = lector.nextInt();
        try {
            while (i < raf.length()) {
                if (raf.readInt() == id) {
                    existeix = true;
                }
                i += 36;
                raf.seek(i);
            }
            if (existeix) {
                raf.seek((id-1)*36);
                raf.writeInt(-1);
                raf.writeInt(id);
                raf.skipBytes(16);
                raf.writeInt(0);
                raf.writeDouble(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu(raf);
    }

    private static void ex3(RandomAccessFile raf) {
        Scanner lector = new Scanner(System.in);
        int id;
        double salariantic, salarinou, quantitatSumar;
        int i = 0;
        boolean existeix = false;
        System.out.println("Introdueix l'id d'empleat a consultar");
        id = lector.nextInt();
        System.out.println("Introdueix la quantitat que vols sumar al salari");
        quantitatSumar = lector.nextDouble();
        try {
            while (i < raf.length()) {
                if (raf.readInt() == id) {
                    existeix = true;
                }
                i += 36;
                raf.seek(i);
            }
            if (existeix) {
                raf.seek((id - 1) * 36);
                raf.skipBytes(28);
                salariantic = raf.readDouble();
                salarinou = salariantic + quantitatSumar;
                raf.seek(((id - 1) * 36) + 28);
                raf.writeDouble(salarinou);
                raf.close();
                System.out.println("Arxiu modificat satisfactoriament");
                System.out.printf("El sou antic es %s, li hem sumat %s, i ara el sou es de: %f", salariantic, quantitatSumar, salarinou);
            } else {
                System.out.println("No es pot canviar el sou d'un empleat que no existeix");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu(raf);
    }

    private static void ex1(RandomAccessFile raf) {
        Scanner lector = new Scanner(System.in);
        char[] caracters = new char[10];
        int id;
        String apellido;
        System.out.println("Introdueix l'identificador de l'empleat a consultar");
        id = lector.nextInt();
        try {
            raf.seek((id-1)*36);
            System.out.print(raf.readInt()+" ");
            for (int i = 0; i < caracters.length; i++) {
                caracters[i] = raf.readChar();
            }
            apellido = new String(caracters);
            System.out.print(apellido+" ");
            System.out.print(raf.readInt()+" ");
            System.out.print(raf.readDouble()+" \n");
        }catch (Exception e) {
            e.printStackTrace();
        }
        menu(raf);
    }

    private static void ex2(RandomAccessFile raf) {
        Scanner lector = new Scanner(System.in);
        StringBuffer buff;
        int id=0, departament,i=0;
        long posicion;
        String cognom;
        double salari;
        boolean existeix = false, comprovar = false;
        System.out.println("Introdueix el cognom");
        cognom = lector.next();
        while (!comprovar) {
            if (cognom.length() <= 10) {
                comprovar = true;
            } else {
                System.out.println("Introdueix el cognom");
                cognom = lector.next();
            }
        }
        System.out.println("Introdueix el departament");
        departament = lector.nextInt();
        System.out.println("Introdueix el sou");
        salari= lector.nextDouble();
        buff = new StringBuffer(cognom);
        buff.setLength(10);
        try {
            while (i<raf.length()) {
                if (raf.readInt() == id) {
                    System.out.println("Ja existeix l'id");
                    existeix = true;
                }
                i+=36;
                raf.seek(i);
            }
            if (!existeix) {
                posicion = raf.length();
                id= (int) ((posicion + 36) / 36);
                raf.seek(raf.length());
                raf.writeInt(id);
                raf.writeChars(buff.toString());
                raf.writeInt(departament);
                raf.writeDouble(salari);
                raf.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        menu(raf);
    }
}
